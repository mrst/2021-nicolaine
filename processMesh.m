function [G, cellid, faceid, regionnames, regionids, getRegionId, getRegionIndices] = processMesh(filename)
%
%
% SYNOPSIS:
%   function [G, cellid, faceid, regionnames, regionids, getRegionId] = processMesh(filename)
%
% DESCRIPTION:
%
% PARAMETERS:
%   filename - file name (string)
%
% RETURNS:
%   G           - MRST grid
%   cellid      - vector with id number for each cell (zero if not assigned)
%   faceid      - vector with id number for each face (zero if not assigned)
%   regionnames - lists of strings given region names
%   regionids   - lists of id numbers corresponding to regionnames
%   getRegionId - utility function that returns the id number give a region name
%



    tnum = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/MAI/TR3/NUM');
    tnodes = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/MAI/TR3/NOD');

    %% 
    coords = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/NOE/COO');
    nnum = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/NOE/NUM');

    nodetbl.nodes = nnum;
    nodetbl = IndexArray(nodetbl);

    celltbl.cells = tnum;
    celltbl = IndexArray(celltbl);

    vecttbl.vect = [1; 2];
    vecttbl = IndexArray(vecttbl);

    nodevecttbl.nodes = [nnum; nnum];
    nodevecttbl.vect = rldecode([1; 2], numel(nnum)*[1; 1]);
    nodevecttbl = IndexArray(nodevecttbl);

    tnodes = reshape(tnodes, [], 3);

    nodes1 = [tnodes(:, 1); tnodes(:, 2); tnodes(:, 3)];
    nodes2 = [tnodes(:, 2); tnodes(:, 3); tnodes(:, 1)];

    edgecelltbl.nodes1 = nodes1;
    edgecelltbl.nodes2 = nodes2;
    edgecelltbl.cells = repmat(tnum, 3, 1);
    edgecelltbl.order = repmat([1; 2; 3], numel(tnum), 1);
    edgecelltbl = IndexArray(edgecelltbl);

    nodes12 = [nodes1, nodes2];
    nodes12 = sort(nodes12, 2);
    nodes12 = sortrows(nodes12);
    nodes12 = unique(nodes12, 'rows');

    edgetbl.nodes1 = nodes12(:, 1);
    edgetbl.nodes2 = nodes12(:, 2);
    edgetbl.edges = (1 : size(nodes12, 1))';
    edgetbl = IndexArray(edgetbl);

    nn = size(nodes12, 1);
    diredgetbl.nodes1 = [nodes12(:, 1); nodes12(:, 2)];
    diredgetbl.nodes2 = [nodes12(:, 2); nodes12(:, 1)];
    diredgetbl.edges = repmat((1 : nn)', 2, 1);
    diredgetbl.dir = rldecode([1; 2], [nn; nn]);
    diredgetbl = IndexArray(diredgetbl);

    gen = CrossIndexArrayGenerator();
    gen.tbl1 = edgecelltbl;
    gen.tbl2 = diredgetbl;
    gen.mergefds = {'nodes1', 'nodes2'};

    diredgecelltbl = crossIndexArray(edgecelltbl, diredgetbl, {'nodes1', 'nodes2'});
    diredgecelltbl = projIndexArray(diredgecelltbl, {'edges', 'cells', 'dir', 'order'});

    dir1tbl.dir = 1;
    dir1tbl = IndexArray(dir1tbl);

    dir1edgecelltbl = crossIndexArray(diredgecelltbl, dir1tbl, {'dir'});

    dir2tbl.dir = 2;
    dir2tbl = IndexArray(dir2tbl);

    dir2edgecelltbl = crossIndexArray(diredgecelltbl, dir2tbl, {'dir'});

    %% we know the structure of the coords

    celltbl = celltbl.addInd('newcells', (1 : celltbl.num)');

    newcellmap = zeros(max(celltbl.get('cells')), 1);
    newcellmap(celltbl.get('cells')) = celltbl.get('newcells');

    coords = reshape(coords, [], 2);
    nodes.coords = coords;
    nodes.num = size(coords, 1);

    edgetbl = sortIndexArray(edgetbl, {'edges', 'nodes1', 'nodes2'});
    nodes1 = edgetbl.get('nodes1');
    nodes2 = edgetbl.get('nodes2');
    facenodes = [nodes1, nodes2];
    facenodes = reshape(facenodes', [], 1);
    nodePos = 2*ones(edgetbl.num, 1);
    nodePos = [1; 1 + cumsum(nodePos)];

    faces.nodes = facenodes;
    faces.nodePos = nodePos;
    faces.num = edgetbl.num;
    
    neighbors = zeros(edgetbl.num, 2);
    ind1 = dir1edgecelltbl.get('edges');
    ind2 = dir1edgecelltbl.get('cells');
    ind2 = newcellmap(ind2);
    neighbors(ind1, 1) = ind2;
    ind1 = dir2edgecelltbl.get('edges');
    ind2 = dir2edgecelltbl.get('cells');
    ind2 = newcellmap(ind2);
    neighbors(ind1, 2) = ind2;

    faces.neighbors = neighbors;

    orderedgecelltbl = projIndexArray(diredgecelltbl, {'cells', 'order', 'edges'});
    orderedgecelltbl = crossIndexArray(orderedgecelltbl, celltbl, {'cells'});
    orderedgecelltbl = sortIndexArray(orderedgecelltbl, {'newcells', 'order', 'edges', 'cells'});

    [~, n] = rlencode(orderedgecelltbl.get('newcells'));
    cells.facePos = 1 + cumsum([0; n]);
    cells.faces = orderedgecelltbl.get('edges');
    cells.num = celltbl.num;

    G.cells = cells;
    G.faces = faces;
    G.nodes = nodes;
    G.griddim = 2;

    id = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/MAI/TR3/FAM');

    idcelltbl.id = abs(id);
    idcelltbl.cells = tnum;
    idcelltbl = IndexArray(idcelltbl);

    idcelltbl = crossIndexArray(idcelltbl, celltbl, {'cells'});

    cellid = zeros(celltbl.num, 1);
    cellid(idcelltbl.get('newcells')) = idcelltbl.get('id');

    
    %% 
    snum = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/MAI/SE2/NUM');
    snodes = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/MAI/SE2/NOD');
    sid = h5read(filename, '/ENS_MAA/Mesh_1/-0000000000000000001-0000000000000000001/MAI/SE2/FAM');
    snodes = reshape(snodes, [], 2);
    
    sedgetbl.cells = snum;
    sedgetbl.nodes1 = snodes(:, 1);
    sedgetbl.nodes2 = snodes(:, 2);
    sedgetbl.id = abs(sid);
    sedgetbl = IndexArray(sedgetbl);

    sedgetbl = crossIndexArray(sedgetbl, diredgetbl, {'nodes1', 'nodes2'});
    sedgetbl = projIndexArray(sedgetbl, {'cells', 'edges', 'id'});
    
    faceid = zeros(edgetbl.num, 1);
    faceid(sedgetbl.get('edges')) = sedgetbl.get('id');
   
    h = h5info(filename, '/FAS/Mesh_1/ELEME');
    groups = h.Groups;
    regionnames = {};
    regionids = {};
    for ind = 1 : numel(groups)
        res = regexp(groups(ind).Name, 'FAM_-([0-9]*)_(.*)', 'tokens');
        res = res{1};
        res{1} = str2num(res{1});
        regionids{end + 1} = res{1};
        regionnames{end + 1} = res{2};
    end
    
    faceids = unique(faceid);
    cellids = unique(cellid);
    
    getRegionId = @(regionname) getIdFromName(regionname, regionnames, regionids, faceids, cellids);
    getRegionIndices = @(regionname) getRegionIndicesFromName(regionname, faceid, cellid, getRegionId);
    

end

function [id, geotype] = getIdFromName(regionname, regionnames, regionids, faceids, cellids)
    
    [lia, locb] = ismember(regionname, regionnames);
    assert(lia, 'region not found');
    id = regionids{locb};
    
    if ismember(id, faceids)
        geotype = 'set of faces';
    elseif ismember(id, cellids)
        geotype = 'set of cells';
    else
        geotype = 'not recognized';
    end
    
end


function ind = getRegionIndicesFromName(regionname, faceid, cellid, getRegionId)
    [id, geotype] = getRegionId(regionname);
    if contains(geotype, 'cell')
        ind = find(cellid == id);
    elseif contains(geotype, 'face')
        ind = find(faceid == id);
    else
        error('no geotype given')
    end
end
