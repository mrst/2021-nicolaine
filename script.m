clear all
close all

filename = 'data/chc_with_surrounding_hole_tr.med';
% h5disp(filename)

[G, cellid, faceid, regionnames, regionids, getRegionId, getRegionIndices] = processMesh(filename);

%% plot grid
figure
plotGrid(G);

%% plot grid with id number
figure
plotCellData(G, cellid);

%% Retrieve outer faces

outerfacenames = {'left_edge_', 'right_edge_bcX', 'bottom_edgeY', 'top_edge_bcY'};
for ind = 1 : numel(outerfacenames)
    outerfacename = outerfacenames{ind};
    outerfaces{ind} = getRegionIndices(outerfacename);
end


%% test plot

figure
clf
plotGrid(G, 'facecolor', 'none');
colors = {'red', 'blue', 'green', 'magenta'};
for i = 1 : 4
    % c = sum(G.faces.neighbors(outerfaces{i}, :), 2);
    plotFaces(G, outerfaces{i}, 'edgecolor', colors{i}, 'linewidth', 3);
end

%% retrieve the domains

outer_region = getRegionIndices('outer_region');
hole        = getRegionIndices('hole');
casing      = getRegionIndices('casing');
cement      = getRegionIndices('cement');
rock        = getRegionIndices('rock');

save('mesh.mat', 'G', 'outerfaces', 'outer_region', 'hole', 'casing', 'cement', 'rock');

%% plot example : we retrieve and plot the cells of the region 'hole'
[id, geotype] = getRegionId('hole');
myholecell = find(cellid == id);


%% plot example : we retrieve and plot the faces of the region 'top_edge_bcY'

[id, geotype] = getRegionId('top_edge_bcY');
topedgefaces = find(faceid == id);

figure
plotGrid(G, 'facecolor', 'none');
plotFaces(G, topedgefaces, 'linewidth', 3, 'edgecolor', 'blue');

